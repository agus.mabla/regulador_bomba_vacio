#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <LiquidCrystal.h>
#include "menu.h"
#include "boton.h"
/* 
   Generales
*/
// Tiempo debounce botones
#define TIEMPO_DEBOUNCE 450

// Presion inicio
#define BARRERA_ACTIVACION_INICIAL -20

// Presion Corte 
#define BARRERA_CORTE_INICIAL -40

//limites
#define BARRERA_MAXIMO 10
#define BARRERA_MINIMO -60
// Aumento/Decremento de presion por click
#define CLICK_MULTIPLIER 1
//Convertion de unidades
#define DIVISOR_PA_UNIDAD 1330
String unidad = " cm/hg";
/*
-------------------------

    Bomba Vacio
*/
#define PIN_BOMBA_VACIO 6

/* 
   LCD
*/
#define rs 12
#define en 11
#define d4 5
#define d5 4
#define d6 3
#define d7 2

/* 
-------------------------

    Sensor Presion Barometrica

      SDA --> Analog 4
      SCL --> Analog 5
     
-------------------------

    Botones
*/
#define PIN_BTN_ARRIBA 7
#define PIN_BTN_ABAJO 8
#define PIN_BTN_DERECHA 9
#define PIN_BTN_IZQUIERDA 10
#define PIN_BTN_OK 13


#define DEBUG 0




Adafruit_BMP085 bmp;

LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
Boton arriba(PIN_BTN_ARRIBA, TIEMPO_DEBOUNCE);
Boton abajo(PIN_BTN_ABAJO, TIEMPO_DEBOUNCE);
Boton izquierda(PIN_BTN_IZQUIERDA, TIEMPO_DEBOUNCE);
Boton derecha(PIN_BTN_DERECHA, TIEMPO_DEBOUNCE);
Boton ok(PIN_BTN_OK, TIEMPO_DEBOUNCE);

int pres_ref = 75;
Menu menu(lcd,BARRERA_ACTIVACION_INICIAL,BARRERA_CORTE_INICIAL,CLICK_MULTIPLIER,BARRERA_MAXIMO,BARRERA_MINIMO, pres_ref, unidad);

void setup() {
  if(DEBUG){
    Serial.begin(9600);
  }
  //Inicializo el LCD
  lcd.begin(16,2);
  //Inicializo el sensor de presion barometrica
  if(!bmp.begin()){
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Error Conectando");
      lcd.setCursor(2,1);
      lcd.print("Con el Sensor");
      if(DEBUG)
        Serial.println("Errror conectando con el sensor");
      while(true);
  }
  arriba.begin();
  abajo.begin();
  izquierda.begin();
  derecha.begin();
  ok.begin();
  pinMode(PIN_BOMBA_VACIO, OUTPUT);
  digitalWrite(PIN_BOMBA_VACIO,0);
  menu.mostrar_pantalla();
}

void loop() {
    int depres =   floor(bmp.readPressure()/DIVISOR_PA_UNIDAD) - pres_ref;
    if(DEBUG){
      Serial.print("Depresion: ");
      Serial.print(depres);
      Serial.println(unidad);
    }
    if(arriba.click()){
      menu.subir_pantalla();
      if(DEBUG){
        Serial.println("Boton arriba presionado");
      }
    }
    if(abajo.click()){
      menu.bajar_pantalla();
      if(DEBUG){
        Serial.println("Boton abajo presionado");
      }
    }
    if(derecha.click()){
      menu.derecha();
      if(DEBUG){
        Serial.println("Boton derecha presionado");
      }
    }
    if(izquierda.click()){
      menu.izquierda();
      if(DEBUG){
        Serial.println("Boton izquierda presionado");
      }
    }
    if(ok.click()){
      if(DEBUG){
        Serial.println("Boton ok presionado");
      }
      if(menu.ok()){
        pres_ref = floor(bmp.readPressure()/DIVISOR_PA_UNIDAD);
        menu.actualizar_pres_ref(pres_ref);
        if(DEBUG){
        Serial.println("Presion de referencia fijada");
        Serial.print("Presion de referencia: ");
        Serial.print(pres_ref);
        Serial.println(unidad);
      }
      }
    }
    menu.actualizar_depresion(depres);
    
    if(menu.estado()){
      if(depres > menu.presion_activacion ){
        prender_bomba();
        if(DEBUG){
          Serial.println("Estado bomba: Activada");
        }
      }else if(depres < menu.presion_corte){
        apagar_bomba();
        if(DEBUG){
          Serial.println("Estado Bomba: Desactivada");
        }
      }
      
    }else{
      apagar_bomba();
    }
}
