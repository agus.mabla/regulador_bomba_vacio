class Menu {
  //Unidad en la que se encuentra operando el sistema
  
    // Pantalla 0: Fijar presion atmosferica de referencia
    // Pantalla 1: Barrera activacion
    // Pantalla 2: Barrera corte
    // Pantalla 3: Activar/Desactivar Sist
  private:
    String unidad = ""; 
    byte n_pantalla = 0;
    const byte cant_pantallas = 4;
    LiquidCrystal pantalla;


    bool encendido;
    byte multiplicador;
    int barrera_maximo;
    int barrera_minimo;
    int pres_ref;
    
  public:
    void mostrar_pantalla() {
      switch (n_pantalla) {
        case 0:
          pantalla.clear();
          pantalla.setCursor(0, 0);
          pantalla.print("Pres ref:");
          pantalla.print(pres_ref);
          pantalla.print(unidad);
          pantalla.setCursor(0, 1);
          pantalla.println("OK-->actualizar ");
          break;

        case 1:
          pantalla.clear();
          pantalla.setCursor(0, 0);
          pantalla.print("Pres Activacion");
          pantalla.setCursor(4, 1);
          pantalla.print(presion_activacion);
          pantalla.print(unidad);
          break;

        case 2:
          pantalla.clear();
          pantalla.setCursor(1, 0);
          pantalla.print("Presion Corte");
          pantalla.setCursor(4, 1);
          pantalla.print(presion_corte);
          pantalla.print(unidad);
          break;

        case 3:
          pantalla.clear();
          pantalla.setCursor(0, 0);
          if (encendido) {
            pantalla.print("Sist Encendido");
          }
          else {
            pantalla.print("Sist Apagado");
          }
          pantalla.setCursor(0, 1);
          pantalla.print("Pres Act ");
          pantalla.print(presion_actual);
          pantalla.print(unidad);
          break;
      }
    }

    int presion_activacion;
    int presion_corte;
    int presion_actual;
    

    Menu(LiquidCrystal& pant, int _presion_activacion, int _presion_corte, byte _multiplicador, int _barrera_maximo, int _barrera_minimo, int _pres_ref, String _unidad): pantalla(pant) {
      pantalla = pant;
      pantalla.begin(16, 2);
      presion_activacion = _presion_activacion;
      presion_corte = _presion_corte;
      multiplicador = _multiplicador;
      barrera_maximo = _barrera_maximo;
      barrera_minimo = _barrera_minimo;
      pres_ref = _pres_ref;
      unidad = _unidad;
    }

    void subir_pantalla() {
      n_pantalla = ((n_pantalla + 1)%cant_pantallas) ;
      mostrar_pantalla();
    }

    void bajar_pantalla() {
      if(n_pantalla == 0){
        n_pantalla = cant_pantallas - 1;
      }else{
        n_pantalla--;
      } 
      mostrar_pantalla();
    }

    void derecha() {
      switch (n_pantalla) {
        case 1:
          if (presion_activacion < barrera_maximo) {
            presion_activacion += multiplicador;
          }
          mostrar_pantalla();
          break;
        case 2:
          if (presion_corte < barrera_maximo) {
            presion_corte += multiplicador;
          }
          mostrar_pantalla();
          break;

        default:
          break;
      }
    }

    void izquierda() {
      switch (n_pantalla) {
        case 1:
          if (presion_activacion > barrera_minimo) {
            presion_activacion -= multiplicador;
          }
          mostrar_pantalla();
          break;
        case 2:
          if (presion_corte > barrera_minimo) {
            presion_corte -= multiplicador;
          }
          mostrar_pantalla();
          break;
        default:
          break;
      }
    }

    bool ok() {
      if (n_pantalla == 0) {
        return true;
      } else if (n_pantalla == 3) {
        encendido = !encendido;
        mostrar_pantalla();
        return false;
      }
    }

    void actualizar_depresion(int pres) {
      if (presion_actual != pres) {
        presion_actual = pres;
        mostrar_pantalla();
      }
    }

    bool estado() {
      return encendido;
    }
    void actualizar_pres_ref(int pres){
      pres_ref = pres;
      mostrar_pantalla();
    }
};
