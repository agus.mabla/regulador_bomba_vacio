#define DEBUG 0
class Boton{
  private: 
    byte _pin;
    unsigned long _ultima_vez =0;
    int _debounce_time;
  public:
    Boton(byte pin,int debounce_time){
      _pin = pin;
      _debounce_time = debounce_time;
    }

    void begin(void){
       pinMode(_pin, INPUT);
    }

    bool click(){
       bool presionado = digitalRead(_pin);
       bool tiempo_cumplido = (millis() > (_ultima_vez + _debounce_time));
       if(DEBUG){
         Serial.print("Estado pin ");
         Serial.print(_pin);
         Serial.print(" : ");
         Serial.println(presionado);
         Serial.print("Tiempo debounce cumplido: ");
         Serial.println(tiempo_cumplido);
       }
       if(presionado && tiempo_cumplido){
          _ultima_vez = millis();
          return true;
       }else{
          return false; 
       }
    }
 };
